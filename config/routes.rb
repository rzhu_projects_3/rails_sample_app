Rails.application.routes.draw do
  get 'sessions/new'
  get '/signup',  to: 'users#new'
  get '/help',  to: 'static_pages#help'
  get '/about',  to: 'static_pages#about'
  get '/contact',  to: 'static_pages#contact'
  get 'static_pages/home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "static_pages#home"
  get '/application/hello', to: 'application#hello'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  resources :users

  get '/login', to: "sessions#new"
  post '/login', to: "sessions#create"
  delete '/logout', to: "sessions#destroy"
end
